//1.
const fibonacci = noOfSequence => {
    const input = [0, 1]; //first input and second input
    for (i = 2; i <= noOfSequence; i++){
        input.push(input[i-2] + input[i-1])
    }
return input
}
console.log('*****FIRST-ANSWER*****')
console.log(fibonacci(9))
console.log('**********************', '\n')
//2.
/* ======converting decimal to binary =========*/
const convertion = number => {
    let binary = "";
    while (number > 0) {
        {number % 2 === 0 ? binary = "0" + binary : binary = "1" + binary}
        number = Math.floor(number / 2);
    }
    return binary
}
console.log('****SECOND-ANSWER*****')
console.log('converting decimal value of 50 to binary = ' + convertion(50));
console.log('converting decimal value of 10 to binary = ' + convertion(10));
console.log('converting decimal value of 16 to binary = ' + convertion(16));
console.log('converting decimal value of 17 to binary = ' + convertion(17));
console.log('**********************', '\n')

//3.
/* =======Prime Factors ===========*/

const primeFactors = num => {
    const primes = [];
    for (let i = 2; i <= num; i++) 
        if ((num % i) === 0) {
            primes.push(i);
            num /= i;
        }
        return primes;
    
}
console.log('*****THIRD-ANSWER*****')
console.log(primeFactors(10));
console.log(primeFactors(50));
console.log(primeFactors(30));
console.log(primeFactors(1500));
console.log('**********************', '\n')

//4.
/* ===== Getting The Sqrt of input ====*/

const getSqrt = num => {
    for (let i = num; i >= 1; i--)
    { (i * i === num) ? num = i : i / num}
    return num
}
console.log('*****FOURTH-ANSWER****')
console.log(getSqrt(9))
console.log(getSqrt(25))
console.log('**********************', '\n')

//5.
/* ======odd number =======*/
const onlyOdd = number => {
    {(number % 2 === 1) ?  console.log("This is Allowed") : console.log("Odd Numbers are only allowed")}
}
console.log('*****FIFTH-ANSWER*****')
console.log(onlyOdd(3))
console.log('**********************', '\n')